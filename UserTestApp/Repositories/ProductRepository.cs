﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserTestApp.Models;

namespace UserTestApp.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        private readonly AvitoContext _db;

        public ProductRepository(AvitoContext _db) 
        {
            this._db = _db;
        }

        public void Create(Product product)
        {
            _db.Products.Add(product);
            Save();
        }

        public void Delete(int id)
        {
            Product product = _db.Products.ElementAtOrDefault(id);
            if (product != null)
                Delete(product);
        }

        public void Delete(Product product)
        {
            if (product != null)
                _db.Products.Remove(product);
            Save();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed;
        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            this._disposed = true;
        }

        public async Task<Product> Find(int id)
        {
            return await _db.Products.FindAsync(id);
        }

        public async Task<IEnumerable<Product>> FindAll()
        {
            return await _db.Products.ToListAsync();
        }

        public async Task<List<Product>> FindAllWithRoles()
        {
            return await _db.Products.
                Include(u => u.SubCategoryId)
                .ToListAsync();
        }

        public async Task<List<Product>> FindAllBySubCategory(int id)
        {
            var products = from p in _db.Products
                        where p.SubCategoryId == id
                        select p;            
            return await products.ToListAsync();
        }

        public async Task<List<Product>> FindAlltProductsBiggerThen(int price)
        {
            var users = _db.Products
                .FromSqlInterpolated($"SELECT * FROM Products WHERE price >= {price}");
            return await users.ToListAsync();
        }

        public async Task<List<Product>> FindAllByName(string Name)
        {
            return await _db.Products.Where
                (u => u.Seller == Name).ToListAsync();
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public void Update(Product product)
        {
            _db.Products.Update(product);
            Save();
        }
    }
}
