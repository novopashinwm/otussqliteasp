using System.ComponentModel.DataAnnotations;
using System.Data.Common;


    public class Category
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
