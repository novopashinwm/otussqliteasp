﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace UserTestApp.Models
{
    public class AvitoContext : DbContext
    {
        static readonly Random  rnd = new Random();
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Product> Products { get; set; }

        public AvitoContext(DbContextOptions<AvitoContext> dbContextOptions)
            : base(dbContextOptions)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();         
        }       

        protected override void OnConfiguring
            (DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                      new Category() { Id = 1, Name = "Транспорт" }
                    , new Category() { Id = 2, Name = "Недвижимость" }
                    , new Category() { Id = 3, Name = "Работа" }
                    , new Category() { Id = 4, Name = "Личные вещи" }
                    , new Category() { Id = 5, Name = "Для дома и дачи" }
                    , new Category() { Id = 6, Name = "Для бизнеса" }
                    , new Category() { Id = 7, Name = "Бытовая электроника" }
                    , new Category() { Id = 8, Name = "Хобби и отдых" }
                    , new Category() { Id = 9, Name = "Животные" }
                    , new Category() { Id = 10, Name = "Услуги" }
                );

            string[][] subcatArr = new string[][]
            {
                new string[] {"1", "Автомобили", "Мотоциклы и мототехника",
                    "Грузовики и спецтехника", "Водный транспорт", "Запчасти и аксессуары" } ,
                new string[] {"2", "Квартиры в новостройках", "Квартиры в аренду",
                    "Квартиры посуточно","Дома, дачи, коттеджи","Комнаты", "Коммерческая недвижимость"  },
                new string[] {"3", "Вакансии", "Отрасли", "Резюме" },
                new string[] {"4", "Одежда, обувь, аксессуары", "Детская одежда и обувь",
                    "Товары для детей и игрушки", "Часы и украшения", "Красота и здоровье" } ,
                new string[] {"5", "Бытовая техника", "Мебель и интерьер",
                        "Посуда и товары для кухни", "Продукты питания", "Ремонт и строительство", "Растения" } ,
                new string[] {"6"  , "Готовый бизнес",  "Оборудование для бизнеса" },
                new string[] {"7", "Аудио и видео", "Игры, приставки и программы",
                        "Настольные компьютеры", "Ноутбуки", "Оргтехника и расходники",
                        "Планшеты и электронные книги", "Телефоны", "Товары для компьютера", "Фототехника" } ,
                new string[] {"8" , "Билеты и путешествия", "Велосипеды","Книги и журналы",
                        "Коллекционирование", "Музыкальные инструменты", "Охота и рыбалка", "Спорт и отдых" },
                new string[] {"9","Собаки","Кошки","Птицы","Аквариум","Другие животные","Товары для животных" },
                new string[] {"10","IT, интернет, телеком","Бытовые услуги","Деловые услуги","Искусство","Красота, здоровье",
                        "Доставка, курьеры","Мастер на час","Няни, сиделки","Оборудование, производство","Обучение, курсы",
                        "Охрана, безопасность","Доставка еды и продуктов" }
            };
            int id = 1;
            for (int i = 0; i < subcatArr.Length; i++)
            {
                int CategoryId = int.Parse(subcatArr[i][0]);
                for (int j = 1; j < subcatArr[i].Length; j++)
                {
                    string item = subcatArr[i][j];
                    modelBuilder.Entity<SubCategory>().HasData(
                        new SubCategory() { Id = id++, Name = item, CategoryId = CategoryId }); ;
                }
            }

            id = 1;
            int subCatId = 1;
            for (int i = 0; i < subcatArr.Length; i++)
            {
                for (int j = 1; j < subcatArr[i].Length; j++)
                {       
                    string item = subcatArr[i][j];
                    var sellers = new string[] {"Анастасия", "Мария", "Дарья", "Анна",
                    "Елизавета" , "Полина", "Виктория", "Екатерина", "Александр",
                    "Максим", "Иван", "Артем", "Дмитрий", "Никита", "Михаил", "Даниил" };
                    Product product = new Product()
                    {
                        Id= id++
                        , DateCreate = DateTime.Now.AddDays(rnd.Next(-100, 0))
                        , Description = $"Описание товара : {item.GetHashCode()}"
                        , Seller = sellers[rnd.Next(0, sellers.Length)]
                        , Phone = $"+79{rnd.Next(100, 999)}{rnd.Next(1_000_000, 10_000_000)}"
                        , SubCategoryId = subCatId++
                        , Price = rnd.Next(1000, 800000)
                        , Name = GetNameProduct()
                    };
                    modelBuilder.Entity<Product>().HasData(product);                   
                }
            }           
        }

        private  string GetNameProduct()
        {
            int len = rnd.Next(3, 15);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                sb.Append((char)('A' + rnd.Next(0, 26)));
            }
            return sb.ToString();
        }

    }
}
