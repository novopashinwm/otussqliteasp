﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using UserTestApp.Models;
using UserTestApp.Repositories;

namespace UserTestApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ProductRepository _db_prod;

        public HomeController(AvitoContext context)
        {
            _db_prod = new ProductRepository(context);
        }

        public async Task<IActionResult> Index()
        { 
            ViewBag.SubCategory = await _db_prod.FindAllBySubCategory(20);
            ViewBag.ProductBigger1000 = await _db_prod.FindAlltProductsBiggerThen(1000);
            ViewBag.ProductVictory = await _db_prod.FindAllByName("Виктория");
            return View(await _db_prod.FindAll());
           
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }       
    }
}
